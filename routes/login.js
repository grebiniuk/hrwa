const path = require('path');
const express = require('express');
const router = express.Router();
const jwt = require('jsonwebtoken');
const users = require('../db/users.json');
const opts = require('../passport.config');

router.get('/', function (req, res) {
  res.sendFile(path.join(__dirname, '../login.html'));
});

router.post('/', function (req, res) {
  const userFromReq = req.body;
  const userInDB = users.find(user => user.login === userFromReq.login);
  if (userInDB && userInDB.password === userFromReq.password) {
    const token = jwt.sign(userFromReq, opts.secretOrKey, { expiresIn: '24h' });
    res.status(200).json({ auth: true, token });
  } else {
    res.status(401).json({ auth: false });
  }
});

module.exports = router;