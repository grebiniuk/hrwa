window.onload = () => {

    const jwt = localStorage.getItem('jwt');
    if (!jwt) {
        location.replace('/login');
    } else {
        const socket = io({
            transportOptions: {
                polling: {
                    extraHeaders: {
                        'x-token': jwt
                    }
                }
            }
        }).connect('http://localhost:3000');

        const gameElem = document.querySelector('#game');
        const pendingElem = document.querySelector('#pending');
        const timerElem = document.querySelector('#timer');
        const playersListElem = document.querySelector('#players-list');
        const textElem = document.getElementById('text');
        document.addEventListener('keypress', function (e) {
            if (currentStage === 'race' && receivedText[index] === e.key) {
                index++;
                textElem.innerHTML = '<mark>' + receivedText.substring(0, index) + '</mark>' + receivedText.substring(index);
                socket.emit('progress', index);
            }
        });
        let receivedText;
        let index = 0;
        let currentStage;


        socket.on('prepare', data => {
            fetch('http://localhost:3000/race/text', {
                headers:{
                    'Authorization': 'Bearer ' + localStorage.getItem('jwt')
                }
            }).then(response => response.text())
            .then(text => receivedText = text);
        });

        socket.on('tick', data => {
            const { timer, stage, players } = data;
            const showPending = !players || stage === 'wait';

            currentStage = stage;
            timerElem.innerText = String(timer) + ' ' + stage;

            if (showPending && pendingElem.matches('.hidden')) {
                pendingElem.className = "shown";
                gameElem.className = "hidden";
            } else if (!showPending && pendingElem.matches('.shown')) {
                pendingElem.className = "hidden";
                gameElem.className = "shown";
                textElem.innerText = receivedText;
                index = 0;

            }

            if (players) {

                playersListElem.innerHTML = players.map(player => {
                    return `<li>${player.login}<span class="progress"><b style="width:${player.progress}%"></b></span></li>`;
                }).join('');
            }
        });
    }
};
