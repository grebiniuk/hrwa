const EventEmitter = require('events');
const texts = require('../db/texts.json');
const {stages, gameTime, waitTime, resultTime} = require('./game-stages');


class Game extends EventEmitter {
  constructor() {
    super();
    this._data = {
      players: [],
      pending: []
    };
    this.on('tick', this.onTick.bind(this));
  }

  turnOn() {
    this.wait();
    this._intervalId = setInterval(() => {
      this.tick();
    }, 1000);
  }

  turnOff() {
    clearInterval(this._intervalId);
  }

  addUser(login) {
    if (!this.hasPendingUser(login)) {
      this._data.pending.push(login);
    }
  }

  removeUser(login) {
    let index = this._data.pending.indexOf(login);
    if (index !== -1) {
      this._data.pending.splice(index, 1);
    }
    index = this._data.players.findIndex(player => player.login === login);
    if (index !== -1) {
      this._data.players.splice(index, 1);
    }
    //if there are no active players on the race or on the result switch to wait
    if (!this._data.players.length && this._data.stage !== stages.wait) {
      this.wait();
    }
  }

  setRaceProgress(login, index) {
    const player = this._data.players.find(player => player.login === login);
    if (player) {
      player.progress = Math.floor((index + 1) / this._data.text.length * 100);
    }
  }

  hasPlayingUser(login) {
    return this._data.players.findIndex(player => player.login === login) !== -1;
  }

  hasPendingUser(login) {
    return this._data.pending.indexOf(login) !== -1;
  }

  onTick() {
    if (this._data.stage === stages.wait && !this._data.pending.length) {
      this._data.timer = waitTime;
    } else if (this._data.timer <= 0) {
      switch (this._data.stage) {
        case stages.wait: {
          this.start();
          break;
        }
        case stages.race: {
          this.stop();
          break;
        }
        case stages.result: {
          this.wait();
          break;
        }
      }
    }
  }

  start() {
    this._data.players = this._data.pending.concat()
      .map(login => { return {login, progress: 0} });
    this._data.pending = [];
    this._data.timer = gameTime;
    this._data.stage = stages.race;
    this.emit('start');
  }

  stop() {
    this._data.timer = resultTime;
    this._data.stage = stages.result;
    this.emit('stop');
  }

  wait() {
    this._data.pending = this._data.pending.concat(
        this._data.players.map(player => player.login));
    this._data.players = [];
    this._data.timer = waitTime;
    this._data.stage = stages.wait;
    this._data.text = texts[Math.floor(Math.random() * texts.length)];
    this.emit('wait');
  }


  tick() {
    this._data.timer -= 1;
    const { timer, stage, players, pending } = this._data;
    this.emit('tick', { timer, stage, players, pending});
  }

  getText() {
    return this._data.stage !== stages.wait ? "" : this._data.text;
  }

  getStage() {
    return this._data.stage;
  }
}

module.exports  = Game;
